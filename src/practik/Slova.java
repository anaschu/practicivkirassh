package practik;
import java.io.Reader;
import java.util.Scanner;

public class Slova {
    public static String[] slova(Reader x) {
        String stroka = "";
        Scanner sc = new Scanner(x);
        String[] zapret = {".", ",", ";", ":", " "}; // символы от которых надо избавится
        while (sc.hasNext()) {
            stroka = stroka + sc.next() + " "; // создание буфкрной строки
        }

        String[] slova;
        slova = stroka.toLowerCase().split(" "); // перевожу все слова из строки в масив при этом сделав все буквы нижнего регистра
        for (int i = 0; i < slova.length; i++) { //удаляю лишние символы из масива
            for (int b = 0; b < zapret.length; b++) {
                while (slova[i].contains(zapret[b])) {
                    slova[i] = slova[i].substring(0, slova[i].length() - 1);
                }
            }

        }

        return slova; // отдаю масив обратно
    }
}




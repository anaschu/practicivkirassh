package Forms_Start;


import TestActi.CsvListener;
import TestActi.ParceListener;
import TestActi.TestActionListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;


public class MainFormAppe {

    public JPanel createContentPane() {
        JPanel totalGUI = new JPanel();
        totalGUI.setLayout(null);


        //Создание надписи на форме(Заголовок)

        JLabel blueLabel = new JLabel("Заголовок");
        JTextField jtfExInf_Int = new JTextField(10);
        blueLabel.setLocation(250,20 );//Где находится
        blueLabel.setSize(100, 100);//размер
        blueLabel.setHorizontalAlignment(0);
        blueLabel.setForeground(Color.blue);//Цвет
        totalGUI.add(blueLabel);


        //Создание кнопок
        JButton redButton = new JButton("Загрузить первый файл");
        redButton.setLocation(10, 200);
        redButton.setSize(180, 30);

        ActionListener actionListener = new TestActionListener();
        redButton.addActionListener(actionListener);
        totalGUI.add(redButton);

        JButton ParceButton = new JButton("Загрузить второй файл");
        ParceButton.setLocation(200, 200);
        ParceButton.setSize(180, 30);
        ActionListener ParceListener = new ParceListener();
        ParceButton.addActionListener(ParceListener);
        totalGUI.add(ParceButton);

        JButton Button3 = new JButton("Результат");
        Button3.setLocation(400, 200);
        Button3.setSize(150,30);
        ActionListener Clistener = new CsvListener();
        Button3.addActionListener(Clistener);
        totalGUI.add(Button3);

        totalGUI.setOpaque(true);
        return totalGUI;

        
    }
}

